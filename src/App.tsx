import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      Loading PWA App...
    </div>
  );
}

export default App;
